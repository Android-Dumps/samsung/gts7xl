#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from gts7xl device
$(call inherit-product, device/samsung/gts7xl/device.mk)

PRODUCT_DEVICE := gts7xl
PRODUCT_NAME := omni_gts7xl
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-T975
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="gts7xlxx-user 11 RP1A.200720.012 T975XXU2DWB2 release-keys"

BUILD_FINGERPRINT := samsung/gts7xlxx/gts7xl:11/RP1A.200720.012/T975XXU2DWB2:user/release-keys
